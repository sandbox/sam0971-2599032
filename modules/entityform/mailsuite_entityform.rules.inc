<?php

/**
 * @file
 * Rules implementation for mailsuite entityform.
 */

/**
 * Implements hook_rules_action_info().
 */
function mailsuite_entityform_rules_action_info() {
  $actions = [];

  $actions['mailsuite_entityform_notify'] = [
    'label' => t('Send Mailsuite EntityForm mail'),
    'parameter' => [
      'submission' => [
        'type' => 'entityform',
        'label' => t('Entityform Submission')
      ],
      'mail_key' => array(
        'type' => 'text',
        'label' => t('Mailkey'),
        'description' => t('Select a mailsuite mailkey.'),
        'options list' => 'mailsuite_get_mail_keys',
        'restriction' => 'input',
      ),
      'submission_mail' => [
        'type' => 'text',
        'label' => t('Entityform Mail to')
      ],
      'submission_values' => [
        'type' => 'text',
        'label' => t('Submission values (use a new line for each value)'),
        'description' => t('Example:') . '<br/>' . 'field_name:Name',
      ],
      'lang' => [
        'type' => 'text',
        'label' => t('Language')
      ],
    ],
    'group' => t('Mailsuite')
  ];

  return $actions;
}

/**
 * Rules action to notify on entity form submission.
 *
 * @param object $submission
 *   The entityforms's submission.
 * @param string $mail_key
 *   The mailsuite mailkey.
 * @param string $submission_mail
 *   The emailaddress where the mail should be send to.
 * @param string $submission_values
 *   The wanted values filled in the rules textarea.
 * @param string $lang
 *   The language the mail should be send in.
 */
function mailsuite_entityform_notify(
  $submission,
  $mail_key,
  $submission_mail,
  $submission_values,
  $lang
) {
  $mail = mailsuite_get_mail_by_key($mail_key, $lang);
  $mailvalues = isset($mail['mail_values']) ? $mail['mail_values'] : [];
  $all_wanted_values = explode(PHP_EOL, $submission_values);

  $wanted_values = [];
  foreach ($all_wanted_values as $wanted_value) {
    $key_value = explode(':', $wanted_value);
    if (count($key_value) == 2) {
      $wanted_values[$key_value[0]] = $key_value[1];
    }
  }

  $form_type = 'entityform';
  $values = mailsuite_convert_values_to_variables(
    $submission,
    $wanted_values,
    $form_type
  );
  $data = ['mailsuite' => []];
  $data['mailsuite']['submission_values'] = $values;
  $params = [];
  $params['subject'] = isset($mailvalues['subject']) ? token_replace(
    $mailvalues['subject'],
    $data
  ) : '';
  $body = isset($mailvalues['body']['value']) ? $mailvalues['body']['value'] : '';
  $params['body'] = token_replace($body, $data);
  drupal_mail(
    'mailsuite',
    $mail_key,
    $submission_mail,
    $lang,
    $params
  );
}
