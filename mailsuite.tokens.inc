<?php

/**
 * @file
 * Token callbacks for the mailsuite module.
 */

/**
 * Implements hook_token_info().
 */
function mailsuite_token_info() {
  $info['types']['mailsuite'] = [
    'name' => 'Mailsuite',
    'description' => t('Tokens related to the Mailsuite module.'),
  ];
  $info['tokens']['mailsuite']['form_values'] = [
    'name' => t('Values from form submission'),
    'description' => t('Values filled in by the site visitor in a form'),
  ];
  $info['tokens']['mailsuite']['receiver_name'] = [
    'name' => t('Receiver name'),
    'description' => t(
      "The e-mail's receiver name, passed through custom form receiver_name"
    ),
  ];

  return $info;
}

/**
 * Implements hook_tokens().
 */
function mailsuite_tokens(
  $type,
  $tokens,
  array $data = [],
  array $options = []
) {

  $replacements = [];

  if ($type == 'mailsuite') {
    foreach ($tokens as $name => $original) {
      $sub_items = explode(':', $name);
      $sub_item = '';
      if (isset($sub_items[0]) && $sub_items[0] == 'form_values') {
        $name = $sub_items[0];
      }
      if (isset($sub_items[1])) {
        $sub_item = $sub_items[1];
      }

      switch ($name) {
        case 'form_values':
          $replace = '';

          if (!empty($sub_item)) {
            if (isset($data['mailsuite']['submission_values'][$sub_item]['value'])) {
              $replace = $data['mailsuite']['submission_values'][$sub_item]['value'];
            }
          }
          else {
            if (isset($data['mailsuite']['submission_values'])) {
              foreach ($data['mailsuite']['submission_values'] as $submission_value) {
                $replace .= '<strong>' . t('!title',
                    ['!title' => $submission_value['title']]
                  ) . '</strong>: ' . $submission_value['value'] . '<br/>';
              }
            }
          }

          $replacements[$original] = $replace;
          break;

        case 'receiver_name':
          $replace = '';
          if (isset($data['mailsuite']['receiver_name'])) {
            $replace = $data['mailsuite']['receiver_name'];
          }

          $replacements[$original] = $replace;
          break;

      }
    }
  }

  return $replacements;
}
