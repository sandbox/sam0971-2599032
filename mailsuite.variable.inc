<?php
/**
 * @file
 * Variable module hook implementations.
 */

/**
 * Implements hook_variable_group_info().
 *
 * We load all emails defined with hook_mailsuite_info and group them by type.
 */
function mailsuite_variable_group_info() {
  $mails = mailsuite_get_mails();
  $groups = [];

  foreach ($mails as $type => $mail) {
    $groups['mailsuite_' . $type] = [
      'title' => t('Mailsuite !type', ['!type' => $type]),
      'description' => t('Mailsuite Mail type'),
      'access' => TRUE,
      'path' => ['admin/config/mailsuite'],
    ];
  }

  return $groups;
}

/**
 * Implements hook_variable_info().
 */
function mailsuite_variable_info() {
  $mails = mailsuite_get_mails();
  $variables = [];

  foreach ($mails as $type => $mailhooks) {
    foreach ($mailhooks as $mailhook => $mail_params) {
      $variables[MAILSUITE_PREFIX . $mailhook . '_[mail_part]'] = array(
        'title' => t('!title', ['!title' => $mail_params['title']]),
        'group' => 'mailsuite_' . $type,
        'localize' => TRUE,
        'build callback' => 'mailsuite_multivar_types',
      );
    }
  }

  return $variables;
}

/**
 * Build a mailsuite multiple variable.
 */
function mailsuite_multivar_types($variable, $options = array()) {
  $name = str_replace('[mail_part]', '', $variable['name']);
  $variable['children'][$name . 'subject']['type'] = 'string';
  $variable['children'][$name . 'body']['type'] = 'text_format';
  $variable = variable_build_multiple($variable, $options);

  return $variable;
}
