# Mailsuite

## Description
This module allows you to administer all your emails in one single place.
With the help of an info hook modules can provide their own emails and
these are useable everywhere in the site.

Emails can also be defined through the interface. Emails defined through
the interface can be exported with features.

There are also submodules like mailsuite_entityform to extend the default
mailsuite functionality.
These modules implement Rules actions to give you the full flexibility
for sending emails whenever you want.

## Currently implemented:
- Send emails from custom forms
- Send emails from entityforms (includes rules action)
- Account emails (drupal defaults)

## Token
- mailsuite:form_values -> Prints all the values defined in wanted_values
- mailsuite:form_values:field_from_form  -> prints a specific field from
the wanted_values
- mailsuite:receiver_name -> If defined in a custom form submit, this will
print the receiver's name

## Features
Emails created through the user interface can be exported with the features
module. The module implements a mailsuitemail entity.

## Modules
### Mailsuite main module
This is the core module. This implements some helper functions and provides
mail support for custom forms.

To enable sending mails from a custom form, you need to add the
mailsuite_mail_this_form function in your submit hook.
This function requires some parameters:
```php
$params['receiver'] = 'user@example.com'; //or load form a user obj.
$params['receiver_name'] = 'User name';
$params['wanted_values'] = [
  'name' => 'Name',
  'email' => 'E-mail',
  'phone' => 'Phone',
  'occasion' => 'Occasion',
  'message' => 'Message'
];
mailsuite_mail_this_form('mailsuite_mailkey', $params, $form_state);
```
### Mailsuite Entityform
Enables support for entity forms. This module implements a rules action.
In this rules action you can define what values from the entityform should be
printed in the e-mail.
Example:
- field_contact_user:Name
- field_contact_firstname:Firstname

It's similar like the wanted_values above:
formfieldkey:Human Readable Label

### Mailsuite Account
Is a small module that puts the drupal build in
account emails in a separate tab on the Mailsuite page.

## Admin page
You can administer all available emails on the website on
admin/config/system/mailsuite.

## Extending Mailsuite
It is very easy to extend Mailsuite. You can define your own emails like this:
```php
/**
 * Implements hook_mailsuite_mail_info().
 */
function hook_mailsuite_mail_info() {
  $mails = [];

  $mails['mymodule_example_mail_1'] = [
    'title' => t('Example mail 1'),
    'description' => t('This is an example email'),
    'type' => 'example',
  ];

  $mails['mymodule_example_mail_2'] = [
    'title' => t('Example mail 2'),
    'description' => t('This is another example email'),
    'type' => 'example',
  ];

  return $mails;
}
```

You can also alter the way how Mailsuite processes the values from a form.
This can be done with the hook_mailsuite_convert_values_to_variables_alter().
You can find an example of this in the mailsuite_entityform module.
