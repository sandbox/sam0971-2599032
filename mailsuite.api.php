<?php

/**
 * @file
 * Hooks provided by Mailsuite module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Implements hook_mailsuite_mail_info().
 */
function hook_mailsuite_mail_info() {
  $mails = [];

  $mails['mymodule_example_mail_1'] = [
    'title' => t('Example mail 1'),
    'description' => t('This is an example email'),
    'type' => 'example',
  ];

  $mails['mymodule_example_mail_2'] = [
    'title' => t('Example mail 2'),
    'description' => t('This is another example email'),
    'type' => 'example',
  ];

  return $mails;
}
