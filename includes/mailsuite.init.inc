<?php

/**
 * @file
 * Contains helper functions for generating emails.
 */

/**
 * Function to get all the mailsuite defined mails.
 *
 * @return array
 *   The mailsuite emails.
 */
function mailsuite_get_mails() {
  $cache = cache_get('mailsuite_mails_info', 'cache');

  if (!isset($cache->data)) {

    $mails = [];

    // Load UI defined emails.
    $entities = mailsuite_load_mailsuite_entities();

    foreach ($entities as $entity) {
      mailsuite_add_mail_to_mails(
        [
          'title' => $entity->title,
          'type' => $entity->type,
          'description' => $entity->description,
          'entity_id' => $entity->id,
        ],
        $entity->machine_name,
        $mails
      );
    }

    $modules = module_implements('mailsuite_mail_info');

    // Get all mails defined with a info hook.
    foreach ($modules as $module) {
      $function_name = $module . '_mailsuite_mail_info';
      $function = $function_name;
      if (function_exists($function)) {
        $module_mails = $function();

        if (count($module_mails)) {
          foreach ($module_mails as $key => $mail) {
            mailsuite_add_mail_to_mails($mail, $key, $mails);
          }
        }
      }
    }

    // Allow other modules to alter existing mails.
    drupal_alter('mailsuite_mails', $mails);

    cache_set('mailsuite_mails_info', $mails);
  }
  else {
    $mails = $cache->data;
  }

  return $mails;
}

/**
 * Helper function that returns all the mailsuite defined mail types.
 *
 * @param string $type
 *   The type of a mailsuite mail.
 *
 * @return array
 *   The found mailtypes that match the $type.
 */
function mailsuite_get_mailtypes($type) {
  $mails = mailsuite_get_mails();
  $type_keys = array_keys($mails);
  $keys = [];

  foreach ($type_keys as $value) {
    if (0 === strpos($value, $type)) {
      $keys[$value] = $value;
    }
  }

  return drupal_json_output($keys);
}

/**
 * Load all the UI defined mailsuite emails.
 *
 * @return array
 *   All the loaded mailsuitemail entities.
 */
function mailsuite_load_mailsuite_entities() {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'mailsuitemail');
  $result = $query->execute();

  if (isset($result['mailsuitemail'])) {
    return entity_load('mailsuitemail', array_keys($result['mailsuitemail']));
  }

  return [];
}

/**
 * Add loaded mail to total mails array.
 *
 * @param array $mail
 *   An array that defines a new mailsuite email.
 * @param string $key
 *   A unique mailsuite machine name.
 * @param array $mails
 *   Array that contains the already existing mailsuite emails.
 */
function mailsuite_add_mail_to_mails(array $mail, $key, array &$mails) {
  $mails[$mail['type']][$key] = mailsuite_fields();
  $mails[$mail['type']][$key]['title'] = $mail['title'];
  $mails[$mail['type']][$key]['description']
    = isset($mail['description']) ? $mail['description'] : '';
  if (isset($mail['entity_id'])) {
    $mails[$mail['type']][$key]['entity_id'] = $mail['entity_id'];
  }

}

/**
 * Load default field setting.
 *
 * @param string $fieldname
 *   The fieldname that exists in a mailsuite email.
 *
 * @return array
 *   Returns the mailsuite field.
 */
function mailsuite_get_default_field($fieldname = NULL) {
  $fields = mailsuite_fields();
  if ($fieldname && isset($fields['fields'][$fieldname])) {
    return $fields['fields'][$fieldname];
  }

  return $fields;
}

/**
 * Mailsuite default fields.
 *
 * @return array
 *   Returns the mailsuite fields.
 */
function mailsuite_fields() {
  return [
    'fields' => [
      'subject' => [
        'type' => 'textfield',
        'title' => t('Subject'),
        'variable_type' => 'string',
        'description' => 'E-mail subject',
      ],
      'body' => [
        'type' => 'text_format',
        'title' => t('Body'),
        'variable_type' => 'text_format',
        'description' => 'E-mail body',
      ]
    ],
  ];
}
