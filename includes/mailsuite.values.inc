<?php

/**
 * @file
 * Convert form values to flat mail values.
 */

/**
 * Loads the filled in admin mail fields.
 *
 * @param array $mail
 *   The mailsuite mail params.
 * @param string $mailkey
 *   The mailsuite defined mailkey.
 *
 * @return array
 *   An array containing the mailsuite variables.
 */
function mailsuite_get_mail_variables(array $mail, $mailkey, $lang = NULL) {
  if (is_null($lang)) {
    $lang = $GLOBALS['language']->language;
  }

  if (!isset($mail['fields'])) {
    return NULL;
  }

  $values = [];

  foreach ($mail['fields'] as $param_key => $field) {
    if (module_exists('i18n_variable') && i18n_variable_list(
        MAILSUITE_PREFIX . $mailkey . '_' . $param_key
      )
    ) {
      $value = i18n_variable_get(
        MAILSUITE_PREFIX . $mailkey . '_' . $param_key,
        $lang
      );
    }
    else {
      $value = variable_get(MAILSUITE_PREFIX . $mailkey . '_' . $param_key);
    }
    $values[$param_key] = $value;
  }

  return $values;
}

/**
 * Helper function to convert form_state values to a generic array.
 *
 * @param mixed $submission
 *   The submission from the form.
 * @param array $wanted_values
 *   The values from the form that the user wants to use in the email.
 * @param string $form_type
 *   Define the source where the submission comes from.
 *
 * @return array
 *   A generic array containing the wanted values from the form.
 */
function mailsuite_convert_values_to_variables(
  $submission,
  array $wanted_values,
  $form_type = 'custom'
) {

  $values = [];

  switch ($form_type) {
    case 'custom':
      if (is_array($submission)) {
        if (isset($submission['values'])) {
          $submission = $submission['values'];
        }

        foreach ($wanted_values as $wanted_key => $wanted_value) {
          if (isset($submission[$wanted_key])) {
            $values[$wanted_key] = [
              'value' => nl2br($submission[$wanted_key]),
              'title' => $wanted_value
            ];
          }
        }
      }
      break;
  }

  drupal_alter(
    'mailsuite_convert_values_to_variables',
    $submission,
    $wanted_values,
    $form_type,
    $values
  );

  return $values;
}
