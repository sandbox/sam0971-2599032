<?php

/**
 * @file
 * Contains mailsuitemail entity.
 */

/**
 * Implements hook_entity_info().
 */
function mailsuite_entity_info() {

  $info = array();

  $info['mailsuitemail'] = array(
    'label' => t('Mailsuite email'),
    'base table' => 'mailsuite',
    'entity keys' => array(
      'id' => 'id',
      'label' => 'title',
      'name' => 'machine_name',
    ),
    'entity class' => 'MailsuiteEntity',
    'controller class' => 'EntityAPIControllerExportable',
    'exportable' => TRUE,
    'uri callback' => 'entity_class_uri',
    'module' => 'mailsuite',
  );

  return $info;
}

/**
 * Form definition for adding / editing a mailsuite email.
 */
function mailsuite_manage_email_form($form, &$form_state, $entity) {

  $form['mailsuitemail'] = array(
    '#type' => 'value',
    '#value' => $entity,
  );

  $form['title'] = array(
    '#title' => t('Mail name'),
    '#type' => 'textfield',
    '#default_value' => isset($entity->title) ? $entity->title : '',
    '#required' => TRUE,
  );

  if (isset($entity->machine_name)) {
    $form['machine_name'] = array(
      '#title' => t('Machinename'),
      '#type' => 'machine_name',
      '#default_value' => isset($entity->machine_name) ? $entity->machine_name : '',
      '#required' => TRUE,
      '#maxlength' => 21,
      '#machine_name' => array(
        'exists' => 'mailsuite_machine_name_exists',
      ),
    );
  }

  $form['type'] = array(
    '#title' => t('Type'),
    '#type' => 'textfield',
    '#default_value' => isset($entity->type) ? $entity->type : '',
    '#required' => TRUE,
    '#autocomplete_path' => 'admin/config/system/mailsuite/autocomplete',
  );

  $form['description'] = array(
    '#title' => t('Mail form description'),
    '#type' => 'textarea',
    '#default_value' => isset($entity->description) ? $entity->description : '',
    '#required' => FALSE,
  );

  $form['original_machine_name'] = [
    '#type' => 'hidden',
    '#default_value' => isset($entity->machine_name) ? $entity->machine_name : '',
  ];

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => isset($entity->id) ? t('Update mail') : t('Save mail'),
    '#weight' => 50,
  );

  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#submit' => array('mailsuitemail_edit_delete'),
    '#weight' => 200,
  );

  return $form;
}

/**
 * Submit handler for the mailsuite add/edit form.
 */
function mailsuite_manage_email_form_submit($form, &$form_state) {
  $entity = $form_state['values']['mailsuitemail'];
  $title = $form_state['values']['title'];
  $description = $form_state['values']['description'];
  $type = $form_state['values']['type'];

  if (isset($form_state['values']['machine_name'])) {
    $machine_name = $form_state['values']['machine_name'];
  }
  else {
    $machine_name = mailsuite_mailsuitemail_get_machine_name($title);
  }

  $entity->title = $title;
  $entity->type = $type;
  $entity->description = $description;
  $entity->machine_name = $machine_name;

  $form_state['redirect'] = 'admin/config/system/mailsuite/' . $type;

  mailsuite_mailsuitemail_save($entity);
  mailsuite_clear_cache();
  drupal_set_message(
    t(
      'Mailsuite email: @name has been saved.',
      array('@name' => $entity->title)
    )
  );
}

/**
 * Convert title to machine name.
 *
 * @param string $title
 *   The filled in mail title.
 *
 * @return string
 *   The machine name.
 */
function mailsuite_mailsuitemail_get_machine_name($title) {
  $machine_name = strtolower($title);
  $machine_name = preg_replace('@[^a-z0-9_]+@', '_', $machine_name);

  return $machine_name;
}

/**
 * Machine name field exists callback.
 *
 * @param string $value
 *   The mail's machine name.
 *
 * @return bool
 *   Return true or false if the machine name exists or not.
 */
function mailsuite_machine_name_exists($value) {
  $mails = mailsuite_get_mails();

  foreach ($mails as $mail_per_type) {
    foreach ($mail_per_type as $existing_machine_name => $mail_data) {
      if ($value == $existing_machine_name) {
        return TRUE;
      }
    }
  }

  return FALSE;
}

/**
 * Mailsuite form delete action.
 *
 * @param array $form
 *   The entity edit form.
 * @param array $form_state
 *   The entity edit form_state.
 */
function mailsuitemail_edit_delete(array $form, array &$form_state) {
  $entity = $form_state['values']['mailsuitemail'];
  mailsuite_mailsuitemail_delete($entity);
  drupal_set_message(
    t(
      'The mail %title has been deleted',
      ['%title' => $entity->title]
    )
  );
  mailsuite_clear_cache();
  $form_state['redirect'] = 'admin/config/system/mailsuite';
}

/**
 * Save mailsuitemail entity.
 *
 * @param object $entity
 *   The mailsuitemail entity.
 */
function mailsuite_mailsuitemail_save(&$entity) {
  entity_get_controller('mailsuitemail')->save($entity);
}

/**
 * Delete mailsuitemail entity.
 *
 * @param object $entity
 *   The mailsuitemail entity.
 */
function mailsuite_mailsuitemail_delete($entity) {
  entity_get_controller('mailsuitemail')->delete([$entity->id]);
}

/**
 * Mailsuite entity class extending the Entity class.
 */
class MailsuiteEntity extends Entity {

}
