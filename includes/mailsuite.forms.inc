<?php

/**
 * @file
 * Mailsuite settings form.
 */

/**
 * Creates a admin settings form to administer all defined mails.
 *
 * @param array $form
 *   The form for the mailsuite admin pages.
 * @param array $form_state
 *   The form_state for the mailsuite admin pages.
 *
 * @return array
 *   System settings form.
 */
function mailsuite_information_settings_form(array $form, array $form_state, $type) {
  $mails = mailsuite_get_mails();
  $langcode = !empty($_GET['variable_realm_key_language']) ? $_GET['variable_realm_key_language'] : $GLOBALS['language']->language;
  $contains_mails = FALSE;

  if (isset($mails[$type])) {
    $mailhooks = $mails[$type];
    $form['email'] = array(
      '#type' => 'vertical_tabs',
    );

    foreach ($mailhooks as $mailhook => $mail_params) {
      $contains_mails = TRUE;

      $form[$mailhook . '_tab'] = [
        '#type' => 'fieldset',
        '#title' => isset($mail_params['title']) ? $mail_params['title'] : $mailhook,
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#group' => 'email',
      ];

      if (user_access(
          'administer mailsuite mails'
        ) && isset($mail_params['entity_id'])
      ) {
        $edit_url = l(
          t('Manage email'),
          'admin/config/system/mailsuite/' . $mail_params['entity_id'] . '/edit'
        );

        $form[$mailhook . '_tab']['mail_title'] = [
          '#theme' => 'html_tag',
          '#tag' => 'h2',
          '#value' => isset($mail_params['title']) ? $mail_params['title'] . '&nbsp;(' . $edit_url . ')' : $mailhook,
        ];
      }

      $form[$mailhook . '_tab']['description'] = [
        '#markup' => $mail_params['description'],
      ];

      foreach ($mail_params['fields'] as $param_key => $mail_param) {
        if (module_exists('i18n_variable') && i18n_variable_list(
            MAILSUITE_PREFIX . $mailhook . '_' . $param_key
          )
        ) {
          $value = i18n_variable_get(
            MAILSUITE_PREFIX . $mailhook . '_' . $param_key,
            $langcode
          );
        }
        else {
          $value = variable_get(MAILSUITE_PREFIX . $mailhook . '_' . $param_key);
        }

        $default_value = $value;

        if (is_array($value) && isset($value['value'])) {
          $default_value = $value['value'];
          $format = $value['format'];
        }

        if (empty($default_value) && isset($mail_param['default_value'])) {
          $default_value = $mail_param['default_value'];
        }

        $form[$mailhook . '_tab'][MAILSUITE_PREFIX . $mailhook . '_' . $param_key] = [
          '#type' => $mail_param['type'],
          '#title' => t('!title', ['!title' => $mail_param['title']]),
          '#default_value' => $default_value,
          '#required' => isset($mail_param['required']) ? $mail_param['required'] : FALSE,
        ];

        if (isset($format)) {
          $form[$mailhook . '_tab'][$mailhook . '_' . $param_key]['#format'] = $format;
        }
      }
    }
  }

  if (!$contains_mails) {
    return [
      'no_content' => [
        '#markup' => t(
          'There are no Mailsuite mails defined. Define them with a hook_mailsuite_mail_info().'
        ),
      ]
    ];
  }

  $form['tokens'] = [
    '#markup' => theme(
      'token_tree',
      [
        'token_types' => ['commerce-order', 'commerce-customer-profile'],
        '#global_types' => TRUE
      ]
    ),
    '#weight' => 99999
  ];

  drupal_alter('mailsuite_information_settings_form', $form);

  return system_settings_form($form);
}

/**
 * Callback for adding a new mailsuite email.
 *
 * @return array|mixed
 *   The mailsuite_manage_email_form.
 */
function mailsuite_mail_add() {
  $entity = entity_get_controller('mailsuitemail')->create();

  return drupal_get_form('mailsuite_manage_email_form', $entity);
}

/**
 * Callback for editing / deleting mailsuite emails.
 *
 * @param int $entity_id
 *   The mailsuitemail entity id.
 *
 * @return mixed
 *   The mailsuite_manage_email_form.
 */
function mailsuite_mail_edit($entity_id) {
  $entity = entity_load_single('mailsuitemail', $entity_id);

  return drupal_get_form('mailsuite_manage_email_form', $entity);
}
