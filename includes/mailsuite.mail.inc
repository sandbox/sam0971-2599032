<?php

/**
 * @file
 * Functions to make it easier to send emails from custom forms.
 */

/**
 * Function to be placed in custom form submit hooks.
 *
 * @param string $mailkey
 *   The mailsuite mailkey.
 * @param array $params
 *   The mailsuite parameters that needs to be send to the mail.
 * @param array $form_state
 *   The form_state from the drupal form.
 * @param string $lang
 *   Optional parameter to set the mail's language.
 */
function mailsuite_mail_this_form(
  $mailkey,
  array $params,
  array &$form_state,
  $lang = NULL
) {
  if (is_null($lang)) {
    $lang = $GLOBALS['language']->language;
  }

  $mail = mailsuite_get_mail_by_key($mailkey, $lang);
  $mailvalues = isset($mail['mail_values']) ? $mail['mail_values'] : [];
  $receiver = isset($params['receiver']) ? $params['receiver'] : variable_get(
    'site_mail',
    ''
  );
  $wanted_values = isset($params['wanted_values']) ? $params['wanted_values'] : [];
  $form_type = isset($params['form_type']) ? $params['form_type'] : 'custom';
  $values = mailsuite_convert_values_to_variables(
    $form_state,
    $wanted_values,
    $form_type
  );

  $data = ['mailsuite' => []];
  if (isset($params['receiver_name'])) {
    $data['mailsuite']['receiver_name'] = $params['receiver_name'];
  }

  $data['mailsuite']['submission_values'] = $values;

  $params = [];
  $params['subject'] = isset($mailvalues['subject']) ? token_replace(
    $mailvalues['subject'],
    $data
  ) : '';
  $body = isset($mailvalues['body']['value']) ? $mailvalues['body']['value'] : '';
  $params['body'] = token_replace($body, $data);
  drupal_mail('mailsuite', $mailkey, $receiver, $lang, $params);
}

/**
 * Helper function to load the mailsuite specifications by key.
 *
 * @param string $mailkey
 *   The mailsuite mailkey.
 *
 * @return array
 *   Returns the found mail parameters.
 */
function mailsuite_get_mail_by_key($mailkey, $lang = NULL) {
  $mails = mailsuite_get_mails();
  foreach ($mails as $params) {
    if (isset($params[$mailkey])) {
      $params[$mailkey]['mail_values'] = mailsuite_get_mail_variables(
        $params[$mailkey],
        $mailkey,
        $lang
      );

      return $params[$mailkey];
    }
  }

  return [];
}
