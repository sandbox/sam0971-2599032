<?php

/**
 * @file
 * Sample htmlmail template provided by the mailsuite module.
 */
?>
<html>
<head>
  <style type="text/css">
    body, table {
      font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
      font-size: 13px;
      margin: 0;
      min-width: 400px;
      overflow-x: hidden;
      overflow-y: scroll;
    }

    h1 {
      display: block;
      font-size: 2em;
      font-weight: bold;
    }

    table {
      border: none;
      width: 100%;
      line-height: 1.5;
    }

    tr {
      display: table-row;
      vertical-align: inherit;
      border-color: inherit;
    }

    blockquote {
      margin: 0 0 0 .8ex;
      border-left: 1px solid #ccc;
      padding-left: 1ex;
    }
  </style>
</head>
<body>
<?php echo $body; ?>
</body>
</html>
